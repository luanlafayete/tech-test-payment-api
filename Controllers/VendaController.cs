using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _11_tech_test_payment_api.Context;
using Microsoft.AspNetCore.Mvc;
using _11_tech_test_payment_api.Entities;

namespace _11_tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendasContext _context;

        public VendaController(VendasContext context)
        {
            _context = context;
        }
       
       [HttpPost]
       public IActionResult CriarVenda(Venda venda)
       {            
            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterVendaPorId), new { id = venda.Id }, venda);
       }       

       [HttpGet]
       public IActionResult ObterVendaPorId(int id)
       {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
            {
                return NotFound();
            }
            return Ok(venda);
       }

        [HttpPut]
        public IActionResult AtualizarStatusVenda(int id, EnumStatusVenda status)
        {
            var statusBanco = _context.Vendas.Find(id);

            if (statusBanco == null)
            {
                return NotFound();
            }
            
            statusBanco.StatusVenda = status;

            _context.Vendas.Update(statusBanco);
            _context.SaveChanges();
            
            return Ok(statusBanco);
        }

    }
}