namespace _11_tech_test_payment_api.Entities
{
    public enum EnumStatusVenda
    {
        Aguardando_Pagamento,
        Pagamento_aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelada
    }
}

//Provável Ateração de Enum para Class
//TODO: Quando for alterar o StatusVenda de Enum para Class, deletar o Migration e o BD para fazer outro BD e uma nova Migration 