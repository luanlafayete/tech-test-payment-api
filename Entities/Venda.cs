using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _11_tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime DataVenda { get; set; }
        public String Item { get; set; }
        public EnumStatusVenda StatusVenda { get; set; }
        public int IdVendedor { get; set; }
        public string Cpf { get; set; }     
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }        
    }
}